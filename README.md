# airgap-wallet

## Prerequisites

- docker
  https://docs.docker.com/docker-for-mac/
- docker-compose
  https://docs.docker.com/compose/


### isntall docker-compose

Install Docker Compose on macOS

**Docker Desktop for Mac** and **Docker Toolbox** already include Compose along with other Docker apps, so Mac users do not need to install Compose separately

https://docs.docker.com/docker-for-mac/install/


### docker load image and set latest tag

```
docker load -i custody-wallet-1.0.1.img.gz
docker tag custody/wallet:1.0.1 custody/wallet:latest
```


## Services

1. Go to directory that has `docker-compose.yml` file.
2. Run `docker-compose up` and Compose creates containers and start services.


### Create containers and start services in the background

```
docker-compose up -d
```


### Restart services

```
docker-compose restart
```


### View and follow log output from containers

```
docker-compose logs -f
```


### Stop services and remove containers

```
docker-compose rm -fs
```
